<?php

/**
 * @file
 * Install, update and uninstall functions for the Maintenance Window module.
 */

/**
 * Implements hook_schema().
 */
function maintenance_windows_schema(): array {
  $schema['maintenance_window'] = [
    'description' => 'Stores maintenance window configuration.',
    'fields' => [
      'id' => [
        'description' => 'Primary key: Unique ID for a maintenance window.',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE,
      ],
      'start' => [
        'description' => 'Start time of the maintenance window.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'end' => [
        'description' => 'End time of the maintenance window.',
        'type' => 'int',
        'not null' => TRUE,
      ],
      'message' => [
        'description' => 'Message to display during the maintenance window.',
        'type' => 'text',
        'not null' => TRUE,
      ],
      'type' => [
        'description' => 'Type of the maintenance window.',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => 'maintenance_mode',
      ],
      'status' => [
        'description' => 'Status of the maintenance window.',
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
      ],
    ],
    'primary key' => ['id'],
  ];

  return $schema;
}
