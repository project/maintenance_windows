<?php

declare(strict_types=1);

namespace Drupal\maintenance_windows;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Logger\LoggerChannelInterface;
use Drupal\Core\State\StateInterface;
use Drupal\maintenance_windows\MaintenanceWindowRepository;

/**
 * Scheduler class, responsible for enabling and disabling maintenance mode.
 */
final class Scheduler {

  /**
   * Constructs a Scheduler object.
   */
  public function __construct(
    private TimeInterface $time,
    private MaintenanceWindowRepository $maintenanceWindowRepository,
    private ConfigFactoryInterface $configFactory,
    private StateInterface $state,
    private LoggerChannelInterface $logger,
  ) {}

  /**
   * Run the cron process to enable or disable maintenance mode.
   */
  public function cron(): void {
    $openingWindows = $this->maintenanceWindowRepository->loadStartingWindows($this->time->getRequestTime());

    foreach ($openingWindows as $window) {
      $this->startWindow($window);
    }

    $closingWindows = $this->maintenanceWindowRepository->loadEndingWindows($this->time->getRequestTime());

    foreach ($closingWindows as $window) {
      $this->endWindow($window);
    }

    $staleWindows = $this->maintenanceWindowRepository->loadMaintenanceWindowsForCleanup($this->time->getRequestTime());

    foreach ($staleWindows as $window) {
      $this->maintenanceWindowRepository->delete($window);
    }

  }

  /**
   * Start maintenance window.
   *
   * @param \Drupal\maintenance_windows\MaintenanceWindow $window
   *   Window.
   */
  public function startWindow(MaintenanceWindow $window) {
    $this->logger->info('Starting @type window: @window...', [
      '@type' => $window->getType(),
      '@window' => $window->getId(),
    ]);

    if ($window->getType() === MaintenanceWindow::TYPE_READ_ONLY_MODE) {
      $this->enableReadonlyMode($window);
    }
    else {
      $this->enableMaintenanceMode($window);
    }

    $this->logger->info('Maintenance window started: @window.', ['@window' => $window->getId()]);
  }

  /**
   * End maintenance window.
   *
   * @param \Drupal\maintenance_windows\MaintenanceWindow $window
   *   Window.
   */
  public function endWindow(MaintenanceWindow $window) {
    $this->logger->info('Ending @type window: @window...', [
      '@type' => $window->getType(),
      '@window' => $window->getId(),
    ]);

    if ($window->getType() === MaintenanceWindow::TYPE_READ_ONLY_MODE) {
      $this->disableReadonlyMode($window);
    }
    else {
      $this->disableMaintenanceMode($window);
    }

    $this->logger->info('Maintenance window ended: @window.', ['@window' => $window->getId()]);
  }

  /**
   * Enable maintenance mode.
   *
   * @param \Drupal\maintenance_windows\MaintenanceWindow $window
   *   Window.
   */
  private function enableMaintenanceMode(MaintenanceWindow $window): void {
    $config = $this->configFactory->getEditable('system.maintenance');
    $config->set('message', $window->getMessage());
    $config->save();

    $this->state->set('system.maintenance_mode', TRUE);

    $window->setStatus(MaintenanceWindow::STATUS_ACTIVE);
    $this->maintenanceWindowRepository->save($window);
    drupal_flush_all_caches();
    $this->logger->info('Caches flushed.');
  }

  /**
   * Disable mainentnance mode.
   *
   * @param \Drupal\maintenance_windows\MaintenanceWindow $window
   *   Window.
   */
  private function disableMaintenanceMode(MaintenanceWindow $window): void {
    $this->state->set('system.maintenance_mode', FALSE);

    $window->setStatus(MaintenanceWindow::STATUS_INACTIVE);
    $this->maintenanceWindowRepository->save($window);
    drupal_flush_all_caches();
    $this->logger->info('Caches flushed.');
  }

  /**
   * Enable readonly mode.
   *
   * @param \Drupal\maintenance_windows\MaintenanceWindow $window
   *   Window.
   */
  private function enableReadonlyMode(MaintenanceWindow $window) {
    $config = $this->configFactory->getEditable('readonlymode.settings');

    $config->set('messages.default', $window->getMessage());
    $config->set('enabled', TRUE);
    $config->save();

    $window->setStatus(MaintenanceWindow::STATUS_ACTIVE);
    $this->maintenanceWindowRepository->save($window);
    drupal_flush_all_caches();
    $this->logger->info('Caches flushed.');
  }

  /**
   * Disable readonly mode.
   *
   * @param \Drupal\maintenance_windows\MaintenanceWindow $window
   *   Window.
   */
  private function disableReadonlyMode(MaintenanceWindow $window) {
    $config = $this->configFactory->getEditable('readonlymode.settings');

    $config->set('enabled', FALSE);
    $config->save();

    $window->setStatus(MaintenanceWindow::STATUS_INACTIVE);
    $this->maintenanceWindowRepository->save($window);
    drupal_flush_all_caches();
    $this->logger->info('Caches flushed.');
  }

}
