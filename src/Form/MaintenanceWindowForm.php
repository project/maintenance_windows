<?php

declare(strict_types=1);

namespace Drupal\maintenance_windows\Form;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\maintenance_windows\MaintenanceWindow;
use Drupal\maintenance_windows\MaintenanceWindowRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a Maintenance Window form.
 */
final class MaintenanceWindowForm extends FormBase {

  /**
   * Constructs a MaintenanceWindowForm object.
   */
  public function __construct(
    protected ModuleHandlerInterface $moduleHandler,
    protected TimeInterface $time,
    protected MaintenanceWindowRepository $maintenanceWindowRepository,
  ) {}

  /**
   * Constructs a MaintenanceWindowForm object.
   */
  public static function create(ContainerInterface $container): self {

    $moduleHandler = $container->get('module_handler');

    $time = $container->get('datetime.time');

    $repository = $container->get('maintenance_windows.repositories');

    return new static(
      $moduleHandler,
      $time,
      $repository
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'maintenance_window_maintenance_window';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MaintenanceWindow $maintenance_window = NULL): array {

    if ($maintenance_window instanceof MaintenanceWindow) {
      $form['id'] = [
        '#type' => 'value',
        '#value' => $maintenance_window->getId(),
      ];
    }

    $config = $this->configFactory()->get('system.date');

    $form['start'] = [
      '#type' => 'datetime',
      '#title' => $this->t('Start'),
      '#required' => TRUE,
      '#default_value' => $maintenance_window ? DrupalDateTime::createFromTimestamp($maintenance_window->getStart()) : NULL,
      '#description' => $this->t('The time that the maintenance window will start using the @timezone timezone.', [
        '@timezone' => $config->get('timezone.default') ?: 'UTC',
      ]),
    ];

    $form['end'] = [
      '#type' => 'datetime',
      '#title' => $this->t('End'),
      '#required' => TRUE,
      '#default_value' => $maintenance_window ? DrupalDateTime::createFromTimestamp($maintenance_window->getEnd()) : NULL,
      '#description' => $this->t('The time that the maintenance window will end using the @timezone timezone.', [
        '@timezone' => $config->get('timezone.default') ?: 'UTC',
      ]),
    ];

    $form['message'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Message'),
      '#required' => TRUE,
      '#default_value' => $maintenance_window ? $maintenance_window->getMessage() : NULL,
    ];

    if (!($this->moduleHandler->moduleExists('readonlymode'))) {
      $form['type'] = [
        '#type' => 'value',
        '#value' => MaintenanceWindow::TYPE_MAINTENANCE_MODE,
      ];
    }
    else {

      $form['type'] = [
        '#type' => 'select',
        '#title' => $this->t('Type'),
        '#options' => [
          MaintenanceWindow::TYPE_MAINTENANCE_MODE => $this->t('Maintenance mode'),
          MaintenanceWindow::TYPE_READ_ONLY_MODE => $this->t('Read Only mode'),
        ],
        '#default_value' => $maintenance_window ? $maintenance_window->getType() : MaintenanceWindow::TYPE_MAINTENANCE_MODE,
        '#required' => TRUE,
      ];

    }

    $form['actions'] = [
      '#type' => 'actions',
      'submit' => [
        '#type' => 'submit',
        '#value' => $this->t('Save'),
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    $start = $form_state->getValue('start')->getTimestamp();
    $end = $form_state->getValue('end')->getTimestamp();

    if ($start >= $end) {
      $form_state->setErrorByName('end', 'The end time must be greater than the start time.');
    }

    if ($start < $this->time->getRequestTime()) {
      $form_state->setErrorByName('start', 'The start time must be in the future.');
    }

    if ($end < $this->time->getRequestTime()) {
      $form_state->setErrorByName('end', 'The end time must be in the future.');
    }

    if ($end - $start > 86400) {
      $form_state->setErrorByName('end', 'The maintenance window must be less than 24 hours.');
    }

    if ($end - $start < 60) {
      $form_state->setErrorByName('end', 'The maintenance window must be at least 1 minute.');
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $maintenance_window = new MaintenanceWindow();
    if ($form_state->getValue('id')) {
      $maintenance_window->setId($form_state->getValue('id'));
    }
    $maintenance_window->setStart($form_state->getValue('start')->getTimestamp());
    $maintenance_window->setEnd($form_state->getValue('end')->getTimestamp());
    $maintenance_window->setMessage($form_state->getValue('message'));
    $maintenance_window->setType($form_state->getValue('type'));
    $maintenance_window->setStatus(MaintenanceWindow::STATUS_INACTIVE);

    $this->maintenanceWindowRepository->save($maintenance_window);

    $this->messenger()->addStatus($this->t('The maintenance window has been scheduled.'));
    $form_state->setRedirect('maintenance_windows.mantenance_window_management');
  }

}
