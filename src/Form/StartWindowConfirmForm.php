<?php

declare(strict_types=1);

namespace Drupal\maintenance_windows\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\maintenance_windows\MaintenanceWindow;
use Drupal\maintenance_windows\Scheduler;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Form to manually start a maintenance window.
 */
final class StartWindowConfirmForm extends ConfirmFormBase {

  public function __construct(protected Scheduler $scheduler) {}

  /**
   * {@inheritDoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('maintenance_windows.scheduler'),
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'maintenance_window_start_window_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to enable this maintenance window?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('maintenance_windows.mantenance_window_management');
  }

  /**
   * {@inheritdoc}
   */
  public function getDescription() {
    return $this->t('This will start the maintenance window.');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MaintenanceWindow $maintenance_window = NULL): array {
    $form = parent::buildForm($form, $form_state);
    $form['#title'] = $this->t('Start maintenance window');
    $form['#description'] = $this->t('This will enable maintenance mode.');

    $form['window'] = [
      '#type' => 'value',
      '#value' => $maintenance_window,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $this->scheduler->startWindow($form_state->getValue('window'));

    $this->messenger()->addStatus($this->t('Done!'));
    $form_state->setRedirect('maintenance_windows.mantenance_window_management');
  }

}
