<?php

declare(strict_types=1);

namespace Drupal\maintenance_windows\Form;

use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Url;
use Drupal\maintenance_windows\MaintenanceWindow;
use Drupal\maintenance_windows\MaintenanceWindowRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Confirmation form to delete a maintenance window.
 */
final class MaintenanceWindowDeleteConfirmForm extends ConfirmFormBase {

  public function __construct(
    protected MaintenanceWindowRepository $maintenanceWindowRepository,
  ) {}

  /**
   * Constructs a MaintenanceWindowDeleteConfirmForm object.
   */
  public static function create(ContainerInterface $container): self {
    return new static(
      $container->get('maintenance_windows.repositories')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'maintenance_window_maintenance_window_delete_confirm';
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion(): TranslatableMarkup {
    return $this->t('Are you sure you want to do this?');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl(): Url {
    return new Url('maintenance_windows.mantenance_window_management');
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, MaintenanceWindow $maintenance_window = NULL) {
    $form = parent::buildForm($form, $form_state);

    $form['maintenance_window'] = [
      '#type' => 'value',
      '#value' => $maintenance_window,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {

    $maintenance_window = $form_state->getValue('maintenance_window');

    $this->maintenanceWindowRepository->delete($maintenance_window);

    $this->messenger()->addStatus($this->t('The maintenance window has been deleted.'));
    $form_state->setRedirect('maintenance_windows.mantenance_window_management');
  }

}
