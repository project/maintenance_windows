<?php

declare(strict_types=1);

namespace Drupal\maintenance_windows;

use Drupal\Core\Database\Connection;
use Drupal\maintenance_windows\MaintenanceWindow;

/**
 * Class MaintenanceWindowFactory.
 */
final class MaintenanceWindowRepository {

  private const TABLE_NAME = 'maintenance_window';

  /**
   * Constructs a MaintenanceWindowFactory object.
   */
  public function __construct(private Connection $connection) {}

  /**
   * Load a maintenance window by its ID.
   *
   * @param int $id
   *   The ID of the maintenance window to load.
   *
   * @return \Drupal\maintenance_windows\MaintenanceWindow|null
   *   The loaded maintenance window, or NULL if not found.
   */
  public function load(int $id): ?MaintenanceWindow {

    $query = $this->connection->select(self::TABLE_NAME, 'mw')
      ->fields('mw')
      ->condition('mw.id', $id)
      ->execute();

    $record = $query->fetchAssoc();

    if ($record) {
      $maintenance_window = new MaintenanceWindow();
      $maintenance_window->setId((int) $record['id']);
      $maintenance_window->setStart((int) $record['start']);
      $maintenance_window->setEnd((int) $record['end']);
      $maintenance_window->setMessage($record['message']);
      $maintenance_window->setType($record['type']);
      $maintenance_window->setStatus((int) $record['status']);

      return $maintenance_window;
    }

    return NULL;
  }

  /**
   * Save a maintenance window to the database.
   *
   * @param \Drupal\maintenance_windows\MaintenanceWindow $maintenance_window
   *   The maintenance window to save.
   */
  public function save(MaintenanceWindow $maintenance_window): void {
    $this->connection->merge(self::TABLE_NAME)
      ->keys(['id' => $maintenance_window->getId()])
      ->fields([
        'start' => $maintenance_window->getStart(),
        'end' => $maintenance_window->getEnd(),
        'message' => $maintenance_window->getMessage(),
        'type' => $maintenance_window->getType(),
        'status' => $maintenance_window->getStatus(),
      ])
      ->execute();
  }

  /**
   * Delete a maintenance window from the database.
   *
   * @param \Drupal\maintenance_windows\MaintenanceWindow $maintenance_window
   *   The maintenance window to delete.
   *
   * @throws \Exception
   */
  public function delete(MaintenanceWindow $maintenance_window): void {
    $this->connection->delete(self::TABLE_NAME)
      ->condition('id', $maintenance_window->getId())
      ->execute();
  }

  /**
   * Load all maintenance windows that are starting at or before the given timestamp.
   *
   * @param int $timestamp
   *   The timestamp to compare against.
   *
   * @return \Drupal\maintenance_windows\MaintenanceWindow[]
   *   An array of maintenance windows.
   */
  public function loadStartingWindows(int $timestamp): array {
    $query = $this->connection->select(self::TABLE_NAME, 'mw')
      ->fields('mw')
      ->condition('mw.status', MaintenanceWindow::STATUS_INACTIVE)
      ->condition('mw.start', $timestamp, '<=')
      ->condition('mw.end', $timestamp, '>')
      ->execute();

    $maintenance_windows = [];

    while ($record = $query->fetchAssoc()) {
      $maintenance_window = new MaintenanceWindow();
      $maintenance_window->setId((int) $record['id']);
      $maintenance_window->setStart((int) $record['start']);
      $maintenance_window->setEnd((int) $record['end']);
      $maintenance_window->setMessage($record['message']);
      $maintenance_window->setType($record['type']);
      $maintenance_window->setStatus((int) $record['status']);

      $maintenance_windows[] = $maintenance_window;
    }

    return $maintenance_windows;
  }

  /**
   * Load all maintenance windows that are ending at or before the given timestamp.
   *
   * @param int $timestamp
   *   The timestamp to compare against.
   *
   * @return \Drupal\maintenance_windows\MaintenanceWindow[]
   *   An array of maintenance windows.
   */
  public function loadEndingWindows(int $timestamp): array {
    $query = $this->connection->select(self::TABLE_NAME, 'mw')
      ->fields('mw')
      ->condition('mw.status', MaintenanceWindow::STATUS_ACTIVE)
      ->condition('mw.end', $timestamp, '<=')
      ->execute();

    $maintenance_windows = [];

    while ($record = $query->fetchAssoc()) {
      $maintenance_window = new MaintenanceWindow();
      $maintenance_window->setId((int) $record['id']);
      $maintenance_window->setStart((int) $record['start']);
      $maintenance_window->setEnd((int) $record['end']);
      $maintenance_window->setMessage($record['message']);
      $maintenance_window->setType($record['type']);
      $maintenance_window->setStatus((int) $record['status']);

      $maintenance_windows[] = $maintenance_window;
    }

    return $maintenance_windows;
  }

  /**
   * Load all maintenance windows that are inactive and have ended 24 hours before the given timestamp.
   *
   * @param int $timestamp
   *   The timestamp to compare against.
   *
   * @return \Drupal\maintenance_windows\MaintenanceWindow[]
   *   An array of maintenance windows.
   */
  public function loadMaintenanceWindowsForCleanup(int $timestamp): array {

    // Subtract one day from the timestamp.
    $timestamp = $timestamp - 86400;

    $query = $this->connection->select(self::TABLE_NAME, 'mw')
      ->fields('mw')
      ->condition('mw.status', MaintenanceWindow::STATUS_INACTIVE)
      ->condition('mw.end', $timestamp, '<')
      ->execute();

    $maintenance_windows = [];

    while ($record = $query->fetchAssoc()) {
      $maintenance_window = new MaintenanceWindow();
      $maintenance_window->setId((int) $record['id']);
      $maintenance_window->setStart((int) $record['start']);
      $maintenance_window->setEnd((int) $record['end']);
      $maintenance_window->setMessage($record['message']);
      $maintenance_window->setType($record['type']);
      $maintenance_window->setStatus((int) $record['status']);

      $maintenance_windows[] = $maintenance_window;
    }

    return $maintenance_windows;
  }

  /**
   * Load all maintenance windows.
   *
   * @return \Drupal\maintenance_windows\MaintenanceWindow[]
   *   An array of maintenance windows.
   */
  public function loadAll(): array {
    $query = $this->connection->select(self::TABLE_NAME, 'mw')
      ->fields('mw')
      ->execute();

    $maintenance_windows = [];

    while ($record = $query->fetchAssoc()) {
      $maintenance_window = new MaintenanceWindow();
      $maintenance_window->setId((int) $record['id']);
      $maintenance_window->setStart((int) $record['start']);
      $maintenance_window->setEnd((int) $record['end']);
      $maintenance_window->setMessage($record['message']);
      $maintenance_window->setType($record['type']);
      $maintenance_window->setStatus((int) $record['status']);

      $maintenance_windows[] = $maintenance_window;
    }

    usort($maintenance_windows, function (MaintenanceWindow $a, MaintenanceWindow $b) {
      return $a->getStart() <=> $b->getStart();
    });

    return $maintenance_windows;
  }

}
