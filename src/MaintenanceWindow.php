<?php

declare(strict_types=1);

namespace Drupal\maintenance_windows;

/**
 * Defines a maintenance window.
 */
class MaintenanceWindow {

  public const STATUS_INACTIVE = 0;

  public const STATUS_ACTIVE = 1;

  public const TYPE_MAINTENANCE_MODE = 'maintenance_mode';

  public const TYPE_READ_ONLY_MODE = 'read_only_mode';

  /**
   * DB Id for the record.
   *
   * @var null|int
   */
  private ?int $id = NULL;

  /**
   * Start time of the maintenance window.
   *
   * @var int
   */
  private int $start;

  /**
   * End time of the maintenance window.
   *
   * @var int
   */
  private int $end;

  /**
   * Message to display during the maintenance window.
   *
   * @var string
   */
  private string $message;

  /**
   * Type of the maintenance window.
   *
   * @var string
   */
  private string $type;

  /**
   * Status of the maintenance window.
   *
   * @var int
   */
  private int $status;

  /**
   * Set the ID of the maintenance window.
   *
   * @param int $id
   *   The ID of the maintenance window.
   */
  public function setId(int $id): void {
    $this->id = $id;
  }

  /**
   * Get the ID of the maintenance window.
   *
   * @return int
   *   The ID of the maintenance window.
   */
  public function getId(): ?int {
    if (!isset($this->id) || $this->id == NULL) {
      return NULL;
    }
    return $this->id;
  }

  /**
   * Set the start time of the maintenance window.
   *
   * @param int $start
   *   The start time of the maintenance window.
   */
  public function setStart(int $start): void {
    $this->start = $start;
  }

  /**
   * Get the start time of the maintenance window.
   *
   * @return int
   *   The start time of the maintenance window.
   */
  public function getStart(): int {
    return $this->start;
  }

  /**
   * Set the end time of the maintenance window.
   *
   * @param int $end
   *   The end time of the maintenance window.
   */
  public function setEnd(int $end): void {
    $this->end = $end;
  }

  /**
   * Get the end time of the maintenance window.
   *
   * @return int
   *   The end time of the maintenance window.
   */
  public function getEnd(): int {
    return $this->end;
  }

  /**
   * Set the message to display during the maintenance window.
   *
   * @param string $message
   *   The message to display during the maintenance window.
   */
  public function setMessage(string $message): void {
    $this->message = $message;
  }

  /**
   * Get the message to display during the maintenance window.
   *
   * @return string
   *   The message to display during the maintenance window.
   */
  public function getMessage(): string {
    return $this->message;
  }

  /**
   * Set the type of the maintenance window.
   *
   * @param string $type
   *   The type of the maintenance window.
   */
  public function setType(string $type): void {
    $this->type = $type;
  }

  /**
   * Get the type of the maintenance window.
   *
   * @return string
   *   The type of the maintenance window.
   */
  public function getType(): string {
    return $this->type;
  }

  /**
   * Set the status of the maintenance window.
   *
   * @param int $status
   *   The status of the maintenance window.
   */
  public function setStatus(int $status): void {
    $this->status = $status;
  }

  /**
   * Get the status of the maintenance window.
   *
   * @return int
   *   The status of the maintenance window.
   */
  public function getStatus(): int {
    return $this->status;
  }

}
