<?php

declare(strict_types=1);

namespace Drupal\maintenance_windows\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Url;
use Drupal\maintenance_windows\MaintenanceWindow;
use Drupal\maintenance_windows\MaintenanceWindowRepository;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Returns responses for Maintenance Window routes.
 */
final class MantenanceWindowManagementController extends ControllerBase {

  /**
   * The controller constructor.
   */
  public function __construct(
    private MaintenanceWindowRepository $maintenanceWindowRepository,
  ) {}

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('maintenance_windows.repositories'),
    );
  }

  /**
   * Builds the response.
   */
  public function __invoke(): array {

    $windows = $this->maintenanceWindowRepository->loadAll();

    $build['add'] = [
      '#type' => 'link',
      '#title' => $this->t('Add maintenance window'),
      '#url' => Url::fromRoute('maintenance_windows.maintenance_window_add'),
      '#attributes' => [
        'class' => ['button button--primary'],
      ],
    ];

    $build['table'] = [
      '#type' => 'table',
      '#header' => [
        'status' => $this->t('Status'),
        'start' => $this->t('Start'),
        'end' => $this->t('End'),
        'type' => $this->t('Type'),
        'operations' => $this->t('Operations'),
      ],
      '#empty' => $this->t('No maintenance windows found.'),
    ];

    foreach ($windows as $window) {

      $start = DrupalDateTime::createFromTimestamp($window->getStart());
      $end = DrupalDateTime::createFromTimestamp($window->getEnd());

      $links = [];

      $links['edit'] = [
        'title' => $this->t('Edit'),
        'url' => Url::fromRoute('maintenance_windows.maintenance_window_edit', ['maintenance_window' => $window->getId()]),
      ];

      if ($window->getStatus() === MaintenanceWindow::STATUS_INACTIVE) {
        $links['start'] = [
          'title' => $this->t('Start'),
          'url' => Url::fromRoute('maintenance_windows.start_window_confirm', ['maintenance_window' => $window->getId()]),
        ];
      }

      if ($window->getStatus() === MaintenanceWindow::STATUS_ACTIVE) {
        $links['end'] = [
          'title' => $this->t('End'),
          'url' => Url::fromRoute('maintenance_windows.end_window_confirm', ['maintenance_window' => $window->getId()]),
        ];
      }

      $links['delete'] = [
        'title' => $this->t('Delete'),
        'url' => Url::fromRoute('maintenance_windows.maintenance_window_delete_confirm', ['maintenance_window' => $window->getId()]),
      ];

      $build['table']['#rows'][$window->getId()] = [
        'status' => $window->getStatus() ? $this->t('Active') : '',
        'start' => $start->format('Y-m-d H:i:s'),
        'end' => $end->format('Y-m-d H:i:s'),
        'type' => $window->getType(),
        'operations' => [
          'data' => [
            '#type' => 'dropbutton',
            '#links' => $links,
          ],
        ],
      ];
    }

    return $build;

  }

}
