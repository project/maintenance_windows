<?php

declare(strict_types=1);

namespace Drupal\maintenance_windows;

use Drupal\Core\ParamConverter\ParamConverterInterface;
use Drupal\maintenance_windows\MaintenanceWindow;
use Drupal\maintenance_windows\MaintenanceWindowRepository;
use Symfony\Component\Routing\Route;

/**
 * Parameter Converter for Drupal Routes to load Maintenance Window objects.
 */
final class MaintenanceWindowParamConverter implements ParamConverterInterface {

  /**
   * Constructs a MaintenanceWindowParamConverter object.
   */
  public function __construct(
    private MaintenanceWindowRepository $maintenanceWindowRepository,
  ) {}

  /**
   * {@inheritdoc}
   */
  public function convert($value, $definition, $name, array $defaults): ?MaintenanceWindow {
    $window = $this->maintenanceWindowRepository->load((int) $value);
    return $window;
  }

  /**
   * {@inheritdoc}
   */
  public function applies($definition, $name, Route $route): bool {
    return isset($definition['type']) && $definition['type'] === 'maintenance_window';
  }

}
