## INTRODUCTION

The Maintenance Window module provides a method of placing
your site into maintenance mode without user interaction.  By default it will simply enable and disable maintenance mode, and update the message displayed to the user on a schedule.  If you have the Read Only Mode module installed, then you can optionally enable and disable Read Only Mode instead.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## MAINTAINERS

Current maintainers for Drupal 10:

- Jason Murray (zenphp) - https://www.drupal.org/u/zenphp

